﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LinqAPP
{
    public static class HttpService
    {
        static readonly HttpClient client = new HttpClient();

        public static async Task<string> GetResponse(string endpoint)
        {
            HttpResponseMessage response = await client.GetAsync(endpoint);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }
    }
}
