﻿using LinqAPP.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LinqAPP.Repository
{
    public static class UserRepository
    {
        public static async Task<List<User>> GetUsersAsync()
        {
            var response = await HttpService.GetResponse("https://bsa-dotnet.azurewebsites.net/api/Users");

            return JsonConvert.DeserializeObject<List<User>>(response);
        }

        public static async Task<User> GetByIdAsync(int id)
        {
            var response = await HttpService.GetResponse($"https://bsa-dotnet.azurewebsites.net/api/Users/{id}");

            return JsonConvert.DeserializeObject<User>(response);
        }
    }
}