﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LinqAPP.Repository
{
    public static class TaskRepository
    {      
        public static async Task<List<Models.Task>> GetTasksAsync()
        {
            var response = await HttpService.GetResponse(" https://bsa-dotnet.azurewebsites.net/api/Tasks");

            return JsonConvert.DeserializeObject<List<Models.Task>>(response);
        }
    }
}
