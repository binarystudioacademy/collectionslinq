﻿using LinqAPP.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LinqAPP.Repository
{
    public static class ProjectRepository
    {
       

        public static async Task<List<Project>> GetProjectsAsync()
        {
            var response = await HttpService.GetResponse("https://bsa-dotnet.azurewebsites.net/api/Projects");

            return JsonConvert.DeserializeObject<List<Project>>(response);
        }

        public static async Task<Project> GetByIdAsync(int id)
        {
            var response = await HttpService.GetResponse($"https://bsa-dotnet.azurewebsites.net/api/Projects/{id}");

            return JsonConvert.DeserializeObject<Project>(response);
        }
               
        
    }
}
