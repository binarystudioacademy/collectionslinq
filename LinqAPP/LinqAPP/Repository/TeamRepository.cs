﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LinqAPP.Repository
{
    public static class TeamRepository
    {        
        public static async Task<List<Models.Team>> GetTeamsAsync()
        {
            var response = await HttpService.GetResponse("https://bsa-dotnet.azurewebsites.net/api/Teams");

            return JsonConvert.DeserializeObject<List<Models.Team>>(response);
        }
    }
}
