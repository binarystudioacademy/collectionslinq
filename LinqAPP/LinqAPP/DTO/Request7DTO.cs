﻿using LinqAPP.Models;

namespace LinqAPP.DTO
{
    public class Request7DTO
    {
        public Project Project { get; set; }
        public Task LongestTaskByDescription { get; set; }
        public Task ShortestTaskByName { get; set; }
        public int UserCount { get; set; }
    }
}
