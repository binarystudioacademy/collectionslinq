﻿using LinqAPP.Models;

namespace LinqAPP.DTO
{
    public class Request6DTO
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int LastProjectTaskCount { get; set; }
        public int UnfulfilledUserTasksCount { get; set; }
        public Task LongestUserTask { get; set; }
    }
}