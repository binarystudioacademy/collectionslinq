﻿using LinqAPP.Models;
using System.Collections.Generic;

namespace LinqAPP.DTO
{
    public  class Request4DTO
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
        public List<User> Users { get; set; }
    }
}
