﻿using LinqAPP.DTO;
using LinqAPP.Models;
using LinqAPP.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinqAPP
{
    internal class Program
    {       
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            var response1 = await Request1(36);
            var response2 = await Request2(108);
            var response3 = await Request3(33);
            var response4 = await Request4();
            var response5 = await Request5();
            var response6 = await Request6(34);
            var response7 = await Request7(6);

            Console.WriteLine("done!");
            Console.ReadKey();
        }

        private static async Task<Dictionary<Project, int>> Request1(int userId)
        {
            var result = new Dictionary<Project, int>();

            var projects = await ProjectRepository.GetProjectsAsync();
            var tasks = await TaskRepository.GetTasksAsync();

            var userProjects = projects.Where(x => x.AuthorId == userId).ToList();
            foreach (var project in userProjects)
            {
                var tasksCount = tasks.Count(x => x.ProjectId == project.Id);
                result.Add(project, tasksCount);
            }

            return result;
        }

        private static async Task<List<Models.Task>> Request2(int userId)
        {
            var tasks = await TaskRepository.GetTasksAsync();
            var result = tasks.Where(x => x.Name.Length < 45 && x.PerformerId == userId).ToList();
            return result;
        }

        private static async Task<Dictionary<int, string>> Request3(int userId)
        {
            DateTime date = new DateTime(2022, 01, 01);
            var tasks = await TaskRepository.GetTasksAsync();

            var result = tasks.Where(x => x.State == TaskState.Finished && x.FinishedAt < date)
                              .Where(x => x.PerformerId == userId)
                              .Select(x => new { x.Id, x.Name })
                              .ToDictionary(x => x.Id, x => x.Name);

            return result;
        }

        private static async Task<List<Request4DTO>> Request4()
        {
            DateTime date = new DateTime(2011, 01, 01);
            var users = await UserRepository.GetUsersAsync();
            var teams = await TeamRepository.GetTeamsAsync();

            var result = teams.GroupJoin(
                                    users,
                                    t => t.Id,
                                    u => u.TeamId,
                                   (team, user) => new Request4DTO
                                   {
                                       Id = team.Id,
                                       TeamName = team.Name,
                                       Users = user.Where(u => u.BirthDay < date).OrderByDescending(x=>x.RegisteredAt).ToList()
                                   }).ToList();

            return result;
        }

        private static async Task<Dictionary<User, List<Models.Task>>> Request5()
        {
            var people = new Dictionary<User, List<Models.Task>>();
            var users = await UserRepository.GetUsersAsync();
            var tasks = await TaskRepository.GetTasksAsync();

            var userslistSort = users.OrderBy(x => x.FirstName).ToList();

            foreach (var item in userslistSort)
            {
                var userTask = tasks.Where(x => x.PerformerId == item.Id).OrderByDescending(x => x.Name.Length).ToList();
                people.Add(item, userTask);
            }

            return people;
        }

        private static async Task<Request6DTO> Request6(int userId)
        {
            var user = await UserRepository.GetByIdAsync(userId);
            var projects = await ProjectRepository.GetProjectsAsync();
            var tasks = await TaskRepository.GetTasksAsync();

            var lastProject = projects.Where(x => x.AuthorId == userId)
                                      .OrderByDescending(x => x.CreatedAt).First();

            var lastProjectTaskCount = tasks.Count(x => x.ProjectId == lastProject.Id);

            var unfinishedUserTasks = tasks.Where(x => x.PerformerId == userId)
                                           .Count(x => x.State == TaskState.Unfinished || x.State == TaskState.Canceled);

            var userTasks = tasks.Where(x => x.PerformerId == userId).ToList();

            return new Request6DTO
            {
                User = user,
                LastProject = lastProject,
                LastProjectTaskCount = lastProjectTaskCount,
                UnfulfilledUserTasksCount = unfinishedUserTasks,
                LongestUserTask = GetLongestUserTask(userTasks)

            };
        }

        private static async Task<Request7DTO> Request7(int projectId)
        {
            var project = await ProjectRepository.GetByIdAsync(projectId);
            var tasks = await TaskRepository.GetTasksAsync();

            var longestTaskByDescription = tasks.Where(x => x.ProjectId == projectId)
                                                .OrderByDescending(x => x.Description.Length).First();

            var shortestTaskByName = tasks.Where(x => x.ProjectId == projectId)
                                                .OrderBy(x => x.Name.Length).First();
            return null;
        }

        private static Models.Task GetLongestUserTask(List<Models.Task> userTasks)
        {
            int? taskId = null;
            TimeSpan? taskDuration = new TimeSpan();

            foreach (var task in userTasks)
            {
                var dutaion = task.FinishedAt - task.CreatedAt;
                if (taskDuration < dutaion)
                {
                    taskDuration = dutaion;
                    taskId = task.Id;
                }
            }
            return userTasks.FirstOrDefault(x => x.Id == taskId);
        }
    }
}
